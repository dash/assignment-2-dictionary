%define dict_head 0

; Takes 2 arguments and creates a new dictionary entry
; %1 - key
; %2 - pointer to value
%macro colon 2
    %%new_entry:
        dq dict_head
        db %1, 0
        %2:
        %define dict_head %%new_entry
%endmacro