%include "lib.inc"
%include "words.inc"
%include "dict.inc"
%define BUFFER_SIZE 256


section .rodata
test_str: db "blablabla", 0
not_found_msg: db "An entry with the specified key was not found", 0
too_long_input_msg: db "The word must be of length <= 255", 0

section .bss
buffer: resb BUFFER_SIZE


section .text
global _start


_start:
    mov rdi, buffer
    mov rsi, BUFFER_SIZE
    call read_word                              
                                                
    test rax, rax                           ; rax contains 0 if read_word was unsuccessful
    jz .too_long_input

    ; rax contains buffer's address
    ; rdx contains buffer's size

    mov rdi, rax
    mov rsi, dict_head
    call find_word

    test rax, rax
    jz .key_not_found

    add rax, 8                              ; now we have the pointer to the key of the entry
                                            
    mov rdi, rax        
    push rdi
    call string_length                      ; calculate key's length to print out the value of the entry
    pop rdi

    add rdi, rax
    inc rdi                                 
    call print_string
    xor rdi, rdi

    call exit
    
    .key_not_found:
        mov rdi, not_found_msg
        call print_error
        mov rdi, 1
        call exit

    .too_long_input:
        mov rdi, too_long_input_msg
        call print_error
        mov rdi, 1
        call exit

    
