%include "lib.inc"

%macro push_regs 1-*
%rep %0
	push %1
	%rotate 1
%endrep
%endmacro

%macro pop_regs 1-*
%rep %0
	%rotate -1
	pop %1
%endrep
%endmacro

section .text

global find_word

; Takes 2 arguments
; Goes through the entire dictionary looking for a matching key. 
; If a matching entry is found, it will return the address 
; of the start of the entry in the dictionary, otherwise it will return 0.

; rdi - pointer to a null-terminated string
; rsi - pointer to the dictionary
find_word:
    xor rax, rax

    push_regs r12, r13

    mov r12, rdi
    mov r13, rsi

    .loop:
        mov rsi, r13
        add rsi, 8                      ; add the size of the pointer to the next entry
        mov rdi, r12
        call string_equals              ; to compare keys                      

        test rax, rax                     ; string equals returns 1 if the strings are equal,
        jne .found                        ; 0 otherwise

        mov r13, [r13]                  ; the string aren't equal, so we move to the next element in the dictionary
        test r13, r13                   ; if its address is 0, we've reached the head of it and failed to find a matching key
        jnz .loop                       ; otherwise continue looking
    
    .fail:
        xor rax, rax
        jmp .exit
    
    .found:
        mov rax, r13

    .exit:
        pop_regs r12, r13
        ret