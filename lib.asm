section .text

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error
 
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60             ; 'exit' syscall number
    syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
        xor rax, rax
    .loop:
        mov sil, [rdi + rax]        ; move what's in rdi's low byte to rsi's low byte
        test sil, sil               ; check if it's string's end
        jz .exit                    ; if it is - return
        inc rax
        jmp .loop
    .exit:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    push rdi                ; save rdi before calling string_length
    call string_length
    pop rsi
    mov rdx, rax            ; size - 1 byte
    mov rdi, 1              ; stdout file descriptor
    mov rax, 1              ; write syscall
    syscall

    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax

    push rdi               ; сохраняем символ в стеке, в rsi кладем указатель на стек
    mov rsi, rsp           ;
    pop rdi

    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    syscall
    
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 10
    jmp print_char


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi                        ; rax contains the number
    xor rsi, rsi                        ; rsi = 0
    mov r8, 10                          ; r8 is divisor
    dec rsp                             ; put on top of stack string terminator - '0'
    mov byte[rsp], 0    

    .loop:
        xor rdx, rdx                    
        div r8                          ; rax / r8 is saved to rax, rax % r8 is saved to rdx
        dec rsp                         ; put on top of stack new digit
        inc rsi
        add dl, '0'
        mov [rsp], dl

        test rax, rax                   ; as long as rax > 0, jump .loop
        jnz .loop                       ; else print result
    
    .print:
        mov rdi, rsp
        push rsi
        call print_string
        pop rsi

    .clear_stack:
        add rsp, rsi
        inc rsp

    .exit:
        ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi               ; if the number is greater than 0, call print_uint
    jns print_uint

    push rdi                    ; else print - 
    mov rdi, '-'
    call print_char

    pop rdi                     ; call print_uint(neg(rdi))
    neg rdi
    jmp print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax                        ; rdi and rsi contain pointers to strings
    xor rdx, rdx
    xor rcx, rcx

    .loop:
        mov al, [rdi + rcx]
        cmp al, [rsi + rcx]

        jne .not_equal

        inc rcx
        cmp al, 0
        jne .loop

        xor rax, rax
        inc rax
        ret


    .not_equal:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax                ; syscall
    xor rdi, rdi                ; file descriptor

    dec rsp                     
    mov rsi, rsp                ; pointer to stack
    mov rdx, 1                  ; input size

    syscall

    test rax, rax               ; rax contains 0 if it's the end of stream
    jz .end

    mov al, [rsp]

    .end:
        inc rsp
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    push r13
    push r14

    mov r12, rdi
    mov r13, rsi
    xor r14, r14

    .read_leading_spaces:
        
        call read_char

        cmp rax, ` `
        je .read_leading_spaces

        cmp rax, `\n`
        je .read_leading_spaces

        cmp rax, `\t`
        je .read_leading_spaces

        test rax, rax
        jz .error
    
    .loop:
        cmp rax, ` `
        je .end

        cmp rax, `\n`
        je .end

        cmp rax, `\t`
        je .end

        cmp r14, r13
        je .not_enough_space

        mov qword[r12 + r14], rax
        inc r14

        
        call read_char

        test rax, rax
        jne .loop

    .end:
        cmp r13, r14
        je .error
        mov rax, r12
        mov byte[r12 + r14], 0
        mov rdx, r14

        pop r14
        pop r13
        pop r12
        ret

    .not_enough_space:
        pop r14
        pop r13
        pop r12
        xor rax, rax
        ret

    .error:
        pop r14
        pop r13
        pop r12
        xor rax, rax
        xor rdx, rdx
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor rcx, rcx
    mov r8, 10

  .loop:
      mov dl, byte[rdi + rcx]

      sub dl, '0'

      cmp dl, 0
      jb .exit

      cmp dl, 9
      ja .exit
      
      imul rax, r8
      add rax, rdx

      inc rcx
      jmp .loop 

  .exit:
    mov rdx, rcx
    ret

    
; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    push rbx

    xor rax, rax
    xor rdx, rdx

    mov bl, byte[rdi]

    cmp bl, '0'
    jb .parse_sign

    call parse_uint
    pop rbx
    ret

    .parse_sign:
        inc rdi
        call parse_uint
        inc rdx
        cmp bl, '-'
        
        je .neg
        pop rbx
        ret
    
    .neg:
        neg rax
        pop rbx
        ret



; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor r8, r8                  ; r8 is a counter

    .loop: 
        
        mov al, byte[rdi]

        inc rdi
        inc r8                  ; we read 1 new symbol

        cmp r8, rdx             ; check if there is no space in buffer
        jg .not_enough_space

        mov byte[rsi], al
        inc rsi
        jmp .loop

    .success:
        mov rax, r8
        ret

    .not_enough_space:
        xor rax, rax
        ret

print_error:
    mov rsi, rdi
    push rsi
    call string_length
    mov rdx, rax
    mov rax, 1
    mov rdi, 2
    pop rsi
    syscall
    ret