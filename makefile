ASM = nasm
ASMFLAGS = -felf64
PYTHON = python

%.o: %.asm
	$(ASM) $(ASMFLAGS) $< -o $@

dict.o: dict.asm lib.inc colon.inc

main.o: main.asm lib.inc dict.inc words.inc

program: main.o lib.o dict.o
	ld -o $@ $^

.PHONY: run test clean

clean:
	rm *.o

run:
	./program
	
test:
	$(PYTHON) test.py
