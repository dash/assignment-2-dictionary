import subprocess


inputs = ["nebo", "solntse", "a" * 258]
outputs = ["sky", "An entry with the specified key was not found", "The word must be of length <= 255"]
in_stdout = [True, False, False]

all_passed = True

for i in range (len(inputs)):
    process = subprocess.Popen(["./program"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = process.communicate(inputs[i])

    if in_stdout[i] and stdout != outputs[i]:
        all_passed = False
    if not in_stdout[i] and stderr != outputs[i]:
        all_passed = False


if all_passed:
    print("All tests passed")
else:
    print("Not all tests passed")